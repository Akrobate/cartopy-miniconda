FROM continuumio/miniconda3

RUN conda create --name python36 python=3.6
RUN conda install --name python36 -c conda-forge jupyterlab
RUN conda install --name python36 -c conda-forge notebook

RUN conda install --name python36 cartopy
RUN conda install --name python36 pandas
RUN conda install --name python36 numpy
RUN conda install --name python36 seaborn
RUN conda install --name python36 scikit-learn

# All steps of conda install will be remplaces with this single line
# RUN conda create --name python36 --file conda_requirements.txt

COPY .jupyter /root/.jupyter
RUN mkdir /data
WORKDIR /data
