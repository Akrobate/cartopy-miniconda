#!/bin/bash

DOCKER_IMAGE_NAME="miniconde-carto"
PORT="8080"

COMMAND_START_JUPYTER="conda run --name python36 jupyter notebook --ip=0.0.0.0 --port=$PORT --allow-root"

docker build -t $DOCKER_IMAGE_NAME .
docker run -p $PORT:$PORT -v `pwd`/data:/data -v `pwd`/.jupyter:/root/.jupyter -i -t $DOCKER_IMAGE_NAME bash

