# CartoPy Miniconda

docker build -t miniconde-carto .

docker run -i -t miniconde-carto bash

conda run --name python36 jupyter notebook password

conda run --name python36 jupyter notebook --ip=0.0.0.0 --port=8080 --allow-root
